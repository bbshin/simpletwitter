<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${loginUser.account}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessage">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="settings" method="post"><br />

			<input name="id" value="${editUser.id}" id="id" type="hidden" />

			<label for="name">名前</label>
			<input name="name" value="${editUser.name}" id="name">名前はあなたの公開プロフィールに表示されます）<br />

			<label for="account">アカウント名</label>
			<input name="account" value="${editUser.account}" /><br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /><br />

			<label for="email">メールアドレス</label>
			<input name="email" value="${editUser.email}" id="email" /><br />

			<label for="description">説明</label>
			<textarea name="description" cols="35" rows="5" id="description"><c:out value="${editUser.description}" /></textarea><br />

			<input type="submit" value="登録" /><br />
			<a href="./">戻る</a>
		</form>
		<div class="copyright"> Copyright(c) Matsunaga Shinya</div>
	</div>
</body>
</html>