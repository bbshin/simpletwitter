<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages}">
			<div class="errorMessage">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post"><br />
			<label for="accountOrEmail">アカウント名かメールアドレス</label>
			<input name="accountOrEmail" id="accountOrEmail" value="${formRetain}" /><br />
			<c:remove var="formRetain" scope="session" />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /><br />

			<input type="submit" value="ログイン" /><br />
			<a href = "./">戻る</a>
		</form>

		<div class="copyright"> Copyright(c)Matsunaga Shinya</div>

	</div>
</body>
</html>