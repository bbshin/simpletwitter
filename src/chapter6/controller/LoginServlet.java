package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.LoginService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String accountOrEmail = request.getParameter("accountOrEmail");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		//DBにて引数と一致するUser情報を取得
		User user = loginService.login(accountOrEmail, password);

		HttpSession session = request.getSession();

		//formのアカウント情報保持
		session.setAttribute("formRetain", accountOrEmail);

		//userインスタンスに中身があればセッションスコープへ保存しtop.jspへ
		if(user != null) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		//なければエラー文をセッションスコープへ保存しLoginServletへリダイレクト
		}else {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}
	}

}
