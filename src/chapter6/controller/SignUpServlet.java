package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException{

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException{

		List<String> messages = new ArrayList<>();

		HttpSession session = request.getSession();

		//フォームの値を保持
		User retainUser = getRetainUser(request);
		session.setAttribute("retainUser", retainUser);

		//アカウント、パスワードが空かどうかで条件分岐
		if(isValid(request, messages) == true) {

			//フォームからのユーザー情報をDBへ格納、top.jspへ
			User user = new User();
			user.setName(request.getParameter("name"));
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("email"));
			user.setEmail(request.getParameter("email"));
			user.setDescription(request.getParameter("description"));

			new UserService().register(user);

			response.sendRedirect("./");
		}else {
			//空なのでエラーメッセージをセッションスコープに保存しsignup.jspヘリダイレクト
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");

		//空文字であればエラー文をリストに格納
		if(StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}

		if(StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

	private User getRetainUser(HttpServletRequest request) {
		User retainUser = new User();
		retainUser.setName(request.getParameter("name"));
		retainUser.setAccount(request.getParameter("account"));
		retainUser.setEmail(request.getParameter("email"));
		retainUser.setDescription(request.getParameter("description"));

		return retainUser;
	}


}
